# Teste CloudWalk 

Esse repostório contém a Aplicação e (praticamente) todas as suas dependêndencias.

Porém, ainda é necessário instalar o [Docker](https://www.docker.com/community-edition) e o [docker-compose](https://docs.docker.com/compose/).

Uma vez que ambos estejam instalados podemos continuar com a instalação.

Em primeiro lugar, clone este repositório e acesse seu diretório:
```sh
$ cd quake_parser_app
```

Agora é necessário criar os conteineres. Apenas rode o comando abaixo e deixe o resto para o Docker:
```sh
$ docker-compose build
```

Esse comando pode levar um tempo para finalizar.

Após finalizar, suba apenas o banco de PostgreSQL, pois ainda temos que criar os bancos de dados.

```sh
$ docker-compose up postgres
```

O PostgreSQL deve estar no ar. Agora abra outra tab do seu terminal, acesse o dogs e crie o banco de dados. Aproveite para rodar os testes também:
```sh
$ cd quake_parser
$ rake db:create db:migrate
$ rspec
```

Irá criar os bancos de dados de desenvolvimento e o de teste. 

Já rodamos os testes, já criamos o banco de dados e agora vamos rodar a aplicação. Volte para o terminal onde for iniciar o PostgreSQL e dê um CTRL+C para encerrar. Após encerrar o PostgreSQL vamos subir toda a stack.
```sh
$ docker-compose up
```

A aplicaçãp já está no ar. Acesse no navegador localhost:3000.