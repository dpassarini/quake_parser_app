# redis://redis:6379/0
Sidekiq.configure_server do |config|
  config.redis = { url: ENV['REDIS_URL'] || 'redis://0.0.0.0:6379/0'  }
end
