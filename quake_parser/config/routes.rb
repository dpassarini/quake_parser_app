require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq"
  root to: 'home#index'

  get 'home/index'
  get 'ranking/players'

  resources :log_files, only: [:new, :index, :create, :show] do
    get :kills_report, on: :member
    get :kills_report_by_cause, on: :member
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
