class LogFile < ApplicationRecord
  has_one_attached :quake_log
  has_many :matches

  validates :name, presence: true
  validates :quake_log, presence: true

  def finish!
    update(status: :finished)
  end
end
