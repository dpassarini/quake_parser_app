class Match < ApplicationRecord
  has_many :kills
  belongs_to :log_file

  def kills_report 
    {
      name => {
        total_kills: total_kills,
        players: list_players,
        kills: net_players_kills
      }
    }
  end

  def kills_report_by_cause
    {
      name => kills_by_cause
    }
  end

  private 
  def list_players
    kills.only_players.distinct.pluck(:player).sort
  end

  def kills_by_players
    kills.only_players.group(:player).count
  end

  def kills_by_cause
    kills.group(:cause).count
  end

  def kills_by_world
    kills.world_kills.group(:victim).count
  end

  def net_players_kills
    raw_score = kills_by_players
    kills_by_world.each do |player, penalty|
      raw_score[player] = 0 if raw_score[player].nil?

      raw_score[player] -= penalty
    end

    raw_score
  end

  def total_kills
    kills.count
  end
end
