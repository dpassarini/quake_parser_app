class Kill < ApplicationRecord
  belongs_to :match

  scope :only_players, ->{ where.not(player: '<world>') }
  
  scope :world_kills, ->{ where(player: '<world>') }

  def self.ranked_players
    raw_score = only_players.group(:player).count
    
    world_kills.group(:victim).count.each do |player, penalty|
      raw_score[player] = 0 if raw_score[player].nil?

      raw_score[player] -= penalty
    end

    Hash[raw_score.sort_by{ |_, v| -v }]
  end
end
