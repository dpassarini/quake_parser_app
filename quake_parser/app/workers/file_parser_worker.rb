class FileParserWorker
  include Sidekiq::Worker

  def perform(log_file_id)
    log_file = LogFile.find(log_file_id)

    FileParseService.new(log_file.quake_log.download, log_file_id).parse_lines

    log_file.finish!
  end
end
