class LineIdentifierService  
  def model_from_line(line)
    return model_match(line) if line.include?('InitGame')

    return model_kill(line) if line.include?('Kill')
  end

  private
  def model_match(match_line)
    return Match.new(name: match_line.split('\\')[-3])
  end

  def model_kill(kill_line)
    splitted_line = kill_line.split(' ')
    return Kill.new(
      player: find_killer(splitted_line),
      victim: find_victim(splitted_line),
      cause: splitted_line.last
    )
  end

  def find_killer(kill_line)
    killer = []
    kill_line[5..].each do |line_item|
      break if line_item == 'killed'
      killer << line_item
    end

    killer.join(' ')
  end

  def find_victim(kill_line)
    victim = []
    kill_line[..-3].reverse_each do |line_item|
      break if line_item == 'killed'
      victim << line_item
    end

    victim.reverse.join(' ')
  end
end