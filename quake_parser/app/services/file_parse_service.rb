
class FileParseService
  attr_reader :raw_file_data, :lines, :match, :log_file_id
  
  def initialize(raw_file_data, log_file_id)
    @log_file_id = log_file_id
    @raw_file_data = raw_file_data
    @lines = raw_file_data.split("\n")
  end

  def parse_lines
    line_identifier = LineIdentifierService.new
    
    @lines.each do |line|
      entity = line_identifier.model_from_line(line)

      next if entity.nil?
      
      if entity.is_a?(Match)
        entity.log_file_id = log_file_id
        entity.save
        @match = entity
      else # entity is a Kill
        entity.match = @match
        entity.save
      end
    end
  end
end