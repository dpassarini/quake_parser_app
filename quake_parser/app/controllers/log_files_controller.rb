class LogFilesController < ApplicationController
  def index
    @log_files = LogFile.all
  end

  def show
    @log_file = LogFile.find(params[:id])
  end

  def new 
    @log_file = LogFile.new
  end

  def create 
    @log_file = LogFile.new(permitted_params)

    respond_to do |format|
      if @log_file.valid? && @log_file.save
        FileParserWorker.perform_async(@log_file.id)
        format.html { redirect_to @log_file, notice: "Log File was successfully created." }
      else  
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def kills_report
    log_file = LogFile.find(params[:id])
    report = log_file.matches.map(&:kills_report)

    render json: report
  end

  def kills_report_by_cause
    log_file = LogFile.find(params[:id])
    report = log_file.matches.map(&:kills_report_by_cause)

    render json: report
  end

  private
  def permitted_params
    params.require(:log_file).permit(:name, :quake_log)
  end
end