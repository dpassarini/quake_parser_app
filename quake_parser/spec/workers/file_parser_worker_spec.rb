require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe FileParserWorker, type: :worker do
  before do
    Sidekiq::Worker.clear_all
  end

  context 'When receiving a new file model' do 
    before do 
      @log_file = LogFile.new(name: 'log1')
      @log_file.quake_log.attach(io: File.open(file_fixture('game.log')), filename: 'game.log')
      @log_file.save
    end

    it 'should open the file and process the lines' do 
      expect(Match.count).to eq(0)
      expect(Kill.count).to eq(0)
      
      Sidekiq::Testing.inline! do
        FileParserWorker.perform_async(@log_file.id)
      end

      expect(Match.count).to eq(1)
      expect(Kill.count).to eq(29)
    end
  end
end
