require 'rails_helper'

RSpec.describe Kill, type: :model do
  describe '#ranked_players' do
    let!(:fixtures) {FileParseService.new(File.read(file_fixture('game.log'))).parse_lines}

    it 'should bring the full ranking' do
      expected_response = {
        "Oootsimo"=>8, 
        "Zeh"=>7, 
        "Isgalamido"=>3, 
        "Dono da Bola"=>2, 
        "Assasinu Credi"=>1, 
        "Maluquinho"=>0
      }

      expect(Kill.ranked_players).to eq(expected_response)
    end
  end
end
