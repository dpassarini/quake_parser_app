require 'rails_helper'

RSpec.describe Match, type: :model do
  context 'When asks for a report' do
    let!(:fixtures) {FileParseService.new(File.read(file_fixture('game.log'))).parse_lines}

    let(:match) { Match.first }

    it 'should bring the players score, considering the world kill pentalties' do
      expected_response = {
        "baseq3" => {
          :total_kills=>29, 
          :players=>["Assasinu Credi", "Dono da Bola", "Isgalamido", "Maluquinho", "Oootsimo", "Zeh" ], 
          :kills=>{
            "Assasinu Credi"=>1, 
            "Dono da Bola"=>2,
            "Isgalamido"=>3, 
            "Maluquinho"=>0, 
            "Oootsimo"=>8, 
            "Zeh"=>7
          }    
        }
      }
      
      expect(match.kills_report).to eq(expected_response)
    end

    it 'should bring the report by cause' do 
      expected_response = {
        "baseq3" => {
          "MOD_ROCKET"=>5, 
          "MOD_RAILGUN"=>2, 
          "MOD_TRIGGER_HURT"=>3, 
          "MOD_SHOTGUN"=>4, 
          "MOD_MACHINEGUN"=>1, 
          "MOD_ROCKET_SPLASH"=>13, 
          "MOD_FALLING"=>1
        }
      }
        
      expect(match.kills_report_by_cause).to eq(expected_response)
    end
  end
end
