require 'rails_helper'

RSpec.describe LineIdentifierService do
  context 'When parsing file lines' do
    it 'should build a new valid match' do 
      match_line = '1:47 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\0\sv_maxRate\10000\sv_minRate\0\sv_hostname\Code Miner Server\g_gametype\0\sv_privateClients\2\sv_maxclients\16\sv_allowDownload\0\bot_minplayers\0\dmflags\0\fraglimit\20\timelimit\15\g_maxGameClients\0\capturelimit\8\version\ioq3 1.36 linux-x86_64 Apr 12 2009\protocol\68\mapname\q3dm17\gamename\baseq3\g_needpass\0'

      valid_match = subject.model_from_line(match_line)

      expect(valid_match).to be_truthy
    end

    context 'and process a kill' do
      it 'should create a kill with composed name victim' do
        kill_line = '2:37 Kill: 3 2 10: Isgalamido killed Dono da Bola by MOD_RAILGUN'

        kill_model = subject.model_from_line(kill_line)

        expect(kill_model.victim).to eq('Dono da Bola')
      end
      
      it 'should create a kill with composed name killer' do
        kill_line = '3:41 Kill: 2 3 6: Dono da Bola killed Isgalamido by MOD_ROCKET'

        kill_model = subject.model_from_line(kill_line)

        expect(kill_model.player).to eq('Dono da Bola')
      end

      it 'should create a kill with commited by world' do
        world_kill_line = '3:27 Kill: 1022 3 22: <world> killed Isgalamido by MOD_TRIGGER_HURT'

        kill_model = subject.model_from_line(world_kill_line)

        expect(kill_model.player).to eq('<world>')
      end
    end
  end
end