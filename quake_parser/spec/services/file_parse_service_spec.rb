require 'rails_helper'

RSpec.describe FileParseService do
  before do
    @log_file = LogFile.new(name: 'log1')
    @log_file.quake_log.attach(io: File.open(file_fixture('game.log')), filename: 'game.log')
    @log_file.save
    @raw_file_data = @log_file.quake_log.download
  end

  context 'When parsing a log file for only one game' do
    it 'should create one match' do 
      file_parse_service = FileParseService.new(@raw_file_data, @log_file.id)

      expect { file_parse_service.parse_lines }.to change(Match, :count).by(1)
    end

    it 'should create 29 kills for this match' do 
      file_parse_service = FileParseService.new(@raw_file_data, @log_file.id)

      expect { file_parse_service.parse_lines }.to change(Kill, :count).by(29)
    end
  end
end