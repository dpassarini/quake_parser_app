require 'rails_helper'

RSpec.describe "SendLogs", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/send_log/index"
      expect(response).to have_http_status(:success)
    end
  end

end
