class CreateKills < ActiveRecord::Migration[6.1]
  def change
    create_table :kills do |t|
      t.string :player
      t.string :victim
      t.string :cause
      t.references :match, null: false, foreign_key: true

      t.timestamps
    end
  end
end
