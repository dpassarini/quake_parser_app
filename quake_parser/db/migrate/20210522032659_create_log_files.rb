class CreateLogFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :log_files do |t|
      t.string :name

      t.timestamps
    end
  end
end
