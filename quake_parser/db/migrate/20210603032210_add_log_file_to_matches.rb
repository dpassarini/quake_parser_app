class AddLogFileToMatches < ActiveRecord::Migration[6.1]
  def change
    add_reference :matches, :log_file, null: false, foreign_key: true
  end
end
