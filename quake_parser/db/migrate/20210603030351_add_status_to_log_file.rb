class AddStatusToLogFile < ActiveRecord::Migration[6.1]
  def change
    add_column :log_files, :status, :string, default: :pending
  end
end
